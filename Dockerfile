FROM raynera/java
MAINTAINER Andrew Rayner <a.d.rayner@gmail.com>

ENV CONFLUENCE_VERSION 5.7.3
ENV CONFLUENCE_HOME /var/confluence
ENV CONFLUENCE_INSTALL /opt/confluence
ENV CONFLUNECE_DOWNLOAD http://www.atlassian.com/software/confluence/downloads/binary/atlassian-confluence-${CONFLUENCE_VERSION}.tar.gz

RUN mkdir -p ${CONFLUENCE_INSTALL} ${CONFLUENCE_HOME}  \
	&& curl -Lks ${CONFLUNECE_DOWNLOAD} | tar -xzv --strip-components=1 -C ${CONFLUENCE_INSTALL}

COPY entrypoint.sh /entrypoint.sh

RUN useradd -r -u 1000 -s /sbin/nologin atlassian \
	&& chown -R atlassian ${CONFLUENCE_INSTALL} \
	&& chmod +x /entrypoint.sh
   
VOLUME ["${CONFLUENCE_HOME}", "${CONFLUENCE_INSTALL}/logs", "${CONFLUENCE_INSTALL}/temp", "${CONFLUENCE_INSTALL}/work"]

EXPOSE 8090

ENTRYPOINT ["/entrypoint.sh"]
CMD ["run"]
