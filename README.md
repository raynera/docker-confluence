# What is Confluence?

Confluence is team collaboration software. Written in Java and mainly used in corporate environments, it is developed and marketed by Atlassian. 

> [wikipedia.org/wiki/Confluence_(software)](http://en.wikipedia.org/wiki/Confluence_(software))

![logo](http://upload.wikimedia.org/wikipedia/en/thumb/1/1b/Atlassian_Confluence_Logo.svg/150px-Atlassian_Confluence_Logo.svg.png)

## Quick Install

Enter the following to get JIRA up and started quickly:

```bash
/usr/bin/docker run -it --rm --publish 8090:8090 raynera/confluence
```