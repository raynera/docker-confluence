#!/bin/bash

set -e
    
# Set confluence home location
echo "confluence.home=${CONFLUENCE_HOME}" > ${CONFLUENCE_INSTALL}/confluence/WEB-INF/classes/confluence-init.properties

# Fix datetime env issue related to java 8
sed -i 's:gc-`date +%F_%T`:gc-`date +%F`:' /opt/confluence/bin/setenv.sh

# Apply proxy settings if provided
if [ ! -z "${PROXY_URL}" ] ; then
	sed -i "s:disableUploadTimeout=\"true\":disableUploadTimeout=\"true\" proxyName=\"${PROXY_URL}\" proxyPort=\"${PROXY_PORT:-80}\":" ${CONFLUENCE_INSTALL}/conf/server.xml
fi

# Execute confluence
if [ "$1" = 'run' ]; then
	chown -R atlassian ${CONFLUENCE_HOME} ${CONFLUENCE_INSTALL}/logs ${CONFLUENCE_INSTALL}/temp ${CONFLUENCE_INSTALL}/work
  	exec gosu atlassian ${CONFLUENCE_INSTALL}/bin/start-confluence.sh run
fi

exec "$@"
